/*
 * Copyright (c) 2019 Francis-Kim
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#include <Arduino.h>

#define CPU_PWE_ON  PD2
#define SW_PWE_DTT  PC8
#define SYS_LED     PC9

#define I2C_SCL     PB10
#define I2C_SDA     PB11

#define DAC_1_SYNC    

#define BUTTON_UP     PC3
#define BUTTON_DOWN   PC4
#define BUTTON_LEFT   PC5
#define BUTTON_RIGHT  PC6
#define BUTTON_OK     PC7

#define FONT_NAME     u8g2_font_7x13_mf
#define FONT_X        7
#define FONT_Y        16
#define OFFSET_X      0
#define OFFSET_Y      3
#define LCD_WIDHT     128
#define LCD_HEIGHT    64


#endif // __BOARD_H__