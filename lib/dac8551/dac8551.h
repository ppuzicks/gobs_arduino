
#ifndef __DAC8551_H__
#define __DAC8551_H__

#include <Arduino.h>

class DAC8551
{
public:
  DAC8551();
  ~DAC8551();

  void begin();
  void out(uint16_t value);
};

#endif

/**
 * CLOCK ??
 * (CPHA = 1, CPOL = 0) or (CPHA=0, CPOL=1) 
 * DFF 8bit
 * MSB First
 * BIDIMODE=0 RXONLY=0
*/