
#include "dac8551.h"

#include <Arduino.h>
#include <SPI.h>

#define NSS_PIN PA4

DAC8551::DAC8551() 
{
  pinMode(NSS_PIN, OUTPUT);
}

DAC8551::~DAC8551()
{
  SPI.end();
}


void DAC8551::begin()
{
  SPI.setClockDivider(SPI_CLOCK_DIV256);
  SPI.setBitOrder(BitOrder::MSBFIRST);
  SPI.setDataMode(SPI_MODE1);
  SPI.begin();
}

void DAC8551::out(uint16 value)
{
  digitalWrite(NSS_PIN, LOW);
  SPI.transfer(0);
  SPI.transfer(value >> 8);
  SPI.transfer(value & 0xFF);
  //SPI.write((void*)buf, len);
  digitalWrite(NSS_PIN, HIGH);
  //for (int i=0; i<len; i++)
  //  SPI.transfer(buf[i]);
}
