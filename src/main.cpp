#include "pinmap.h"

#include <Arduino.h>
#include <Wire.h>

#include <menu.h>
#include <menuIO/u8g2Out.h>
#include <menuIO/keyIn.h>
#include <menuIO/chainStream.h>
#include <menuIO/serialIn.h>
#include <menuIO/serialOut.h>

#include <dac8551.h>

#define MAX_DEPTH 2

DAC8551 dac8551;
Menu::result menu_start_demo(Menu::eventMask evt, prompt &item);
Menu::result menu_power_off(Menu::eventMask e);

U8G2_SSD1306_128X64_NONAME_1_SW_I2C u8g2(U8G2_R0, I2C_SCL, I2C_SDA, U8X8_PIN_NONE);

const Menu::colorDef<uint8_t> colors[6] MEMMODE = {
  { { 0, 0 }, { 0, 1, 1 } },  // bgColor
  { { 1, 1 }, { 1, 0, 0 } },  // fgColor
  { { 1, 1 }, { 1, 0, 0 } },  // valColor
  { { 1, 1 }, { 1, 0, 0 } },  // unitColor
  { { 0, 1 }, { 0, 0, 1 } },  // cursorColor
  { { 1, 1 }, { 1, 0, 0 } }  // titleColor
};


int select_mode = 0;
SELECT(select_mode, SelMode, "MODE", Menu::doNothing, Menu::noEvent, Menu::noStyle,
  VALUE("pTES", 0, Menu::doNothing, Menu::noEvent),
  VALUE("GIMMY MONEY",    1, Menu::doNothing, Menu::noEvent),
  VALUE("SHOW ME THE MONEY",    2, Menu::doNothing, Menu::noEvent)
);

int choose_current = 1;
CHOOSE(choose_current, ChooCurrent, "AMP", Menu::doNothing, Menu::noEvent, Menu::noStyle,
  VALUE("1mA",  1, Menu::doNothing, Menu::noEvent),
  VALUE("2mA",  2, Menu::doNothing, Menu::noEvent),
  VALUE("3mA",  3, Menu::doNothing, Menu::noEvent),
  VALUE("4mA",  4, Menu::doNothing, Menu::noEvent),
  VALUE("5mA",  5, Menu::doNothing, Menu::noEvent),
  VALUE("6mA",  6, Menu::doNothing, Menu::noEvent),
  VALUE("7mA",  7, Menu::doNothing, Menu::noEvent),
  VALUE("8mA",  8, Menu::doNothing, Menu::noEvent),
  VALUE("9mA",  9, Menu::doNothing, Menu::noEvent)
);

int choose_minutes = 1;
CHOOSE(choose_minutes, ChooMinute, "TIME", Menu::doNothing, Menu::noEvent, Menu::noStyle,
  VALUE("10",  1, Menu::doNothing, Menu::noEvent),
  VALUE("20",  2, Menu::doNothing, Menu::noEvent),
  VALUE("30",  3, Menu::doNothing, Menu::noEvent),
  VALUE("40",  4, Menu::doNothing, Menu::noEvent),
  VALUE("50",  5, Menu::doNothing, Menu::noEvent),
  VALUE("60",  6, Menu::doNothing, Menu::noEvent)
);

MENU(main_menu, "GOBS v2.1", Menu::doNothing, Menu::noEvent, Menu::wrapStyle,
  SUBMENU(SelMode),
  SUBMENU(ChooCurrent),
  SUBMENU(ChooMinute),
  OP("DEMO Start", menu_start_demo, Menu::enterEvent),
  OP("Power Off", menu_power_off, Menu::enterEvent)
);

Menu::serialIn  serial(Serial1);
Menu::keyMap button_map[] = {
  { -BUTTON_OK,     (int8_t)Menu::defaultNavCodes[Menu::enterCmd].ch },
  { -BUTTON_UP,     (int8_t)Menu::defaultNavCodes[Menu::upCmd].ch    },
  { -BUTTON_DOWN,   (int8_t)Menu::defaultNavCodes[Menu::downCmd].ch  },
  { -BUTTON_LEFT,   (int8_t)Menu::defaultNavCodes[Menu::leftCmd].ch  },
  { -BUTTON_RIGHT,  (int8_t)Menu::defaultNavCodes[Menu::rightCmd].ch }
};

Menu::keyIn<5> buttons(button_map);

Menu::menuIn* input_list[] = { &buttons, &serial };
MENU_INPUTS(menu_in, &buttons);

MENU_OUTPUTS(menu_out, MAX_DEPTH,
  U8G2_OUT(u8g2, colors, FONT_X, FONT_Y, OFFSET_X, OFFSET_Y, { 0, 0, LCD_WIDHT/FONT_X, LCD_HEIGHT/FONT_Y }),
  SERIAL_OUT(Serial1)
);

NAVROOT(nav, main_menu, MAX_DEPTH, menu_in, menu_out);

Menu::result MenuIdle(Menu::menuOut& o, Menu::idleEvent e) 
{

  o.clear();

  switch (e) {
  case Menu::idleStart:
    o.println("suspending menu!");
    break;
  case Menu::idling:
    o.println("suspended....");
    break;
  case Menu::idleEnd:
    o.println("resuming menu.");
    break;
  }

  return Menu::proceed;
}

Menu::result menu_power_off(Menu::eventMask e) {
  Serial1.print("menu_power_off : ");
  Serial1.println(e);

  digitalWrite(CPU_PWE_ON, LOW);
  return Menu::proceed;
}

Menu::result do_demo(Menu::menuOut& o, Menu::idleEvent e) {

  // if (e == Menu::idling)
  Serial1.print("do_demo evt : ");
  Serial1.println(e);

  return Menu::proceed;
}

Menu::result menu_start_demo(Menu::eventMask evt, prompt &item) {
  Serial1.print("menu_start_demo : ");
  Serial1.println(evt);

  nav.idleOn(do_demo);

  return Menu::proceed;
}

void setup() {
  // put your setup code here, to run once:
  pinMode(CPU_PWE_ON, OUTPUT);
  digitalWrite(CPU_PWE_ON, HIGH);

  pinMode(SYS_LED, OUTPUT);
  digitalWrite(SYS_LED, HIGH);

  //pinMode(SW_PWE_DTT, INPUT);
  //attachInterrupt(SW_PWE_DTT, power_off, RISING);
  Serial1.begin(9600);
  {
    Serial1.println("GOBS v2.1 start");
    Serial1.flush();

    u8g2.begin();
    u8g2.setFont(FONT_NAME);
    buttons.begin();

    nav.idleTask = MenuIdle;
  }

  dac8551.begin();
}

uint16_t buf[100] = {
32768,34825,36874,38908,40916,42893,44830,46719,48553,50325,
52028,53654,55198,56654,58015,59277,60434,61482,62416,63234,
63931,64506,64955,65277,65470,65535,65470,65277,64955,64506,
63931,63234,62416,61482,60434,59277,58015,56654,55198,53654,
52028,50325,48553,46719,44830,42893,40916,38908,36874,34825,
32768,30710,28661,26627,24619,22642,20705,18816,16982,15210,
13507,11881,10337,8881,7520,6258,5101,4053,3119,2301,
1604,1029,580,258,65,0,65,258,580,1029,
1604,2301,3119,4053,5101,6258,7520,8881,10337,11881,
13507,15210,16982,18816,20705,22642,24619,26627,28661,30710
};


void loop() {
  // put your main code here, to run repeatedly:  
  nav.doInput();

  if (nav.changed(0)) {
    u8g2.firstPage();

    do nav.doOutput(); while (u8g2.nextPage());
  }
  
  for (int i=0; i<100; i++)
    dac8551.out(buf[i]);
  //dac8551.out(buf, 3);
  //delay(100);
}